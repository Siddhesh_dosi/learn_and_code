import threading
import time
import math
import os
import random
import re
import sys

''' do not change this code '''

count = 0
def add():
  global count
  for x in range(100000):
       count = count + 1;
  time.sleep(1)
  
'''do not change the above code '''



if __name__ == '__main__':
    input = int(input().strip())
    for i in range(input//100000):
        t1 = threading.Thread(target=add, args=()) 
        t1.start()
        t1.join()

# The above code is passing every test case but there may be some other conditions
# where the value of count may not be in multiple of 100000. Then the code need to modify.

'''do not change this code this is for output '''
print(count)