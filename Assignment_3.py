'''
The program take balance as an input and does a fixed number of additions and substractions in deposit()
and withdraw() methods via use of Threads. However due to thread synchronization problems the balance 
output is incorrect.bYou should use a locking mechanism to ensure that the Balance is computed properly.
'''

import threading 
balance = 100

def deposit(): 
    global balance
    for i in range(1000000):
        balance = balance + 1
        
def withdraw(): 
    global balance
    for i in range(1000000):
        balance = balance - 1    

if __name__ == "__main__": 
    
    balance = int(input().strip())
    lock = threading.Lock()
    # Appying lock to the threads in order to acheive synchronization between shared data (balance)
    t1 = threading.Thread(target=deposit, args=(lock)) 
    t2 = threading.Thread(target=withdraw, args=(lock)) 
  
    t1.start() 
    t2.start() 
    t1.join() 
    t2.join() 
 
    print("all done: balance = " + str(balance))