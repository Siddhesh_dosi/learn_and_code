/*The goal of this assignment is to send commands from parent to child and child process executing the command and return the result via message queue.

The commands are following * ADD X Y - addition

MUL X Y - multiplication

SUB X Y - substraction

Implement following methods.

//Create a message queue and return the message queue id int create_message_queue(int size) -

//send a command via message queue //msgid - ID of the Message queue int send_command(int msgid, command_t cmd)

//Read the command from the message queue //msgid - ID of the Message queue command_t *recv_command(int msgid)

//Send the result via message queue //msgid - ID of the Message queue int send_result(int msgid, result_t result)

//Read the Result from the message queue //msgid - ID of the Message queue result_t *recv_result(int msgid)

//Delete the message queue void delete_message_queue(int msgid)*/



#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include<sys/msg.h>
void child();
void parent();  
int get_child_exit_status();
int msgid;
int commands=0;

typedef struct command_type
{
	long type;
	//The command to execute
	char command[32];
	//The arguments for the command
	int args[2];
} command_t;
typedef struct result_type 
{
	long type;
	int result;
} result_t;

//WRITE YOUR CODE HERE
//Create a message queue and return the message queue id
int create_message_queue()
{
	key_t key = 0x3243;
    int msgid = msgget(key, 0666 | IPC_CREAT);
    return msgid;
}


//send a command via message queue
//msgid - ID of the Message queue
int send_command(int msgid, command_t cmd)
{
	return msgsnd(msgid, &cmd, sizeof(command_t), 0);
}
//Read the command from the message queue
//msgid - ID of the Message queue
command_t *recv_command(int msgid)
{
    command_t *cmd = (command_t*) malloc(sizeof(command_t));
    msgrcv(msgid, cmd, sizeof(command_t), 1, 0);
    return cmd;
}

//Send the result via message queue
//msgid - ID of the Message queue
int send_result(int msgid, result_t result)
{
    return msgsnd(msgid, &result, sizeof(result_t), 0);
}
//Read the Result from the message queue
//msgid - ID of the Message queue
result_t *recv_result(int msgid)
{
	result_t *result = (result_t *)malloc(sizeof(result_t));
    msgrcv(msgid, result, sizeof(result_t), 2, 0);
    return result;
}
//Delete the message queue
void delete_message_queue(int msgid)
{
    msgctl(msgid, IPC_RMID, 0);	
}
void parent(){

	for(int i=1;i<=commands;i++)	
	{
		command_t *cmd;
      	result_t *result;
      	//WRITE CODE HERE TO READ THE COMMAND FROM INPUT
        cmd = (command_t *)malloc(sizeof(command_t *));
        cmd->type = 1;
        result = (result_t *)malloc(sizeof(result_t *));
        scanf("%s %d %d",  cmd->command,  &(cmd->args[0]),  &(cmd->args[1]));

        //SEND THE COMMAND via send_command
        send_command(msgid, *cmd);

        //RECIEVE THE RESULT from CHILD
        result = recv_result(msgid);
      	
		printf("CMD=%s, RES = %d\n",cmd->command,result->result);
		
	}
	printf("Child exited with status:%d\n",get_child_exit_status());
	delete_message_queue(msgid);

}
void child()
{
	for(int i=1;i<=commands;i++)	
	{
		command_t *cmd;
        //WRITE CODE to RECIEVE THE COMMAND,use recv_command method.
        result_t *res;
        cmd = (command_t *)malloc(sizeof(command_t *));
        res = (result_t *)malloc(sizeof(result_t *));
        res->type = 2;
        
        cmd = recv_command(msgid);
        
        //WRITE CODE to process the command.

        if(strcmp(cmd->command,"ADD") == 0)
        {
             res->result = cmd->args[0]+cmd->args[1];
        }
         if(strcmp(cmd->command,"SUB") == 0)
        {
             res->result = cmd->args[0]-cmd->args[1];
        }
         if(strcmp(cmd->command,"MUL") == 0)
        {
             res->result = cmd->args[0]*cmd->args[1];
        }
        
        //SEND RESULT via send_result
        send_result(msgid,*res);      
      
      
	}

        exit(commands); 
}

//DO NOT MODIFY CODE BELOW
int main(int argc, char* argv[]) 
{ 
	pid_t cid; 
	msgid = create_message_queue(); 
	scanf("%d",&commands);
	if(msgid <= 0 )
	{
		printf("Message Queue creation failed\n");
	}
	cid = fork(); 

	// Parent process 
	if (cid == 0) 
	{ 
		child();
	} else if(cid > 0 )
	{
		parent();
	}
} 

//Get the exit code of the child.
int get_child_exit_status()
{
        int stat;
        wait(&stat);
        return WEXITSTATUS(stat);
}
