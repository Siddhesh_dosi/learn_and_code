/*
int open_file_in_read_mode() - should open file in read mode and return the file descriptor

int open_file_in_read_write_mode( )- open file in read and write mode, return the file descriptor

size_t read_file(int fd,char *buffer,int length) - Read the data from the file, fd - File descriptor buffer -
Buffer where the data needs to be read length - the length of the buffer. 
This function should return the amount of data that has been read.

size_t write_file(int fd,char *data) - Write the data to the file. fd - 
File Descriptor for the file where the data needs to be written. data - Contains the string that needs to be written.
*/

#define OPEN_SUCCESS "File Open Successful\n"
#define OPEN_ERROR "File Open Failed\n"
void create_file();
void open_file();
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/fcntl.h>
#include<string.h>
#include <unistd.h>
#include <sys/wait.h>


//Implement this function and return the file descriptor
int open_file_in_read_mode(char *file_name)
{
    int file_p = open(file_name,O_RDONLY);
    if(file_p > 0)
        //Print this message if the open is successful
        printf(OPEN_SUCCESS); 
    else
        //Print this message when open is failed
        printf(OPEN_ERROR);
    return file_p;
 }
//Implement this function and return the file descriptor
int open_file_in_read_write_mode(char *file_name)
{
    int file_p = open(file_name,O_WRONLY);
    if(file_p > 0)
        //Print this message if the open is successful
        printf(OPEN_SUCCESS); 
    else
        //Print this message when open is failed
        printf(OPEN_ERROR);
    return file_p;
}
//Implement the write_file function, the first parameters is the file descriptor
//returned by open function call and 2nd parameter is the data to be written.
size_t write_file(int fd,char *data)
{
    int length = strlen(data)+1;
    return write(fd,data,length);
}
//Implement the read_file function, the first parameters is the file descriptor
//returned by open function call,
size_t read_file(int fd,char *buffer,int length)
{
    return read(fd,buffer,length);
}

//DO NOT MODIFY THE CODE BELOW

int main(int argc, char *argv[])
{
	char buffer[1024];
    int input_data_length  = 0;
    int create;
    scanf("%s",buffer);
    scanf("%d",&create);
    input_data_length = strlen(buffer);
    
    if(create>0)
    {
        create_file();
    }
        int fd = open_file_in_read_mode("test.txt");
    if(create<=0){
        return 0;
    }    
    if(fd<=0)
        {
            printf("Invalid file descriptor returned by open_file_in_read_mode\n");
        }

    //Test the write, if the write 
    int size=(int)write_file(fd,buffer);
    if(size>0){
        printf("Invalid open mode\n");
    }

    close(fd);    

    fd = open_file_in_read_write_mode("test.txt");

    //Test the write, if the write  works
    size_t write_size=(int)write_file(fd,buffer);
    if(write_size<input_data_length || write_size >(input_data_length + 1)){
        printf("Write Failed\n");
    }
    close(fd);
    fd = open_file_in_read_mode("test.txt");

    char buffer_read[1024];
    size = (int)read_file(fd,buffer_read,1024);
    if(size!=write_size){
        printf("Invalid Size of the data returned\n");
    }
     //buffer_read[input_data_length]= '\0';

    printf("Data Read:%s\n",buffer_read);
    printf("Buffer:%s\n",buffer);
    if(strcmp(buffer,buffer_read))
    {
        printf("Did not recieve correct data from read\n");
    }

}
void create_file()
{
	pid_t cid = fork();
	if(cid == 0 )
	{
		char *args[]={"touch","test.txt",NULL};
		execvp(args[0],args); 
	}
	else
	{
		wait(NULL);
	}
}

