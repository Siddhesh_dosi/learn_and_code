/* ---------------------- Task --------------------------------------------
to create a child process using fork system call and invoke "child()" method
in child process and wait for the child to finish and 
print the child exit code in parent process.
---------------------------------------------------------------------------*/


#include <stdio.h>
#include<stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include<sys/wait.h>

pid_t ppid;
pid_t cpid;

void child();
void parent();

int get_child_exit_status()
{
    //Wait for the child to finish and get the status code.
    int stat;
    wait(&stat);
    int stat2 = WEXITSTATUS(stat);
    return stat2;
}
int main(void)
{  
         
    ppid = getpid();
    cpid = fork();
    if (cpid == 0)
        child();  
    else if(cpid > 0)
        //inside the parent process
        printf("Child exited with status=%d",get_child_exit_status());
}

//DO NOT MODIFY CODE BELOW
void child()
{
        pid_t c_pid = getpid();
        if(c_pid == ppid)
        {
                printf("This is not a child\n");
                return;
        }
        printf("This is a child\n");
        int status;
        scanf("%d",&status);
        exit(status);
}

