/*Implement following methods

create_shared_mem(int size) - Create a shared memory segment of a given size

void write_message(int shmid,char * message) - Store the message on shared memory segment.

char* read_message(int shmid,int length) - Read the message from the shared memory given by shmid

length - is the length of the message to read.

void remove_shared_mem(int shmid) - Remove the shared memory segment
*/

#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
// #define NULL (void*)0
void child();
void parent();  
void process_stage(int stage);
int shmid;
int stages=0;

//--WRITE YOUR CODE BELOW--

int create_shared_mem(int size)
{
    key_t key = ftok("shmfile",65);
    int shmid = shmget(key,size,0666|IPC_CREAT);
    return shmid;
}

void write_message(int shmid, char * message)
{
     char *shared_mem = (char*) shmat(shmid,NULL,0);    
     strcpy(shared_mem,message);
     shmdt(shared_mem);
}

char *read_message(int shmid, int length)
{
    char *shared_mem = (char*) shmat(shmid,NULL,0);
    char *data = (char*)malloc(sizeof(shared_mem));
    strcpy(data,shared_mem);
    shmdt(shared_mem);
    return data;
    
}
void remove_shared_mem(int shmid)
{
    shmctl(shmid,IPC_RMID,NULL);
}
//-----DO NOT MODIFY THE CODE BELOW-----

//Get the exit code of the child.
int get_child_exit_status()
{
        int stat;
        wait(&stat);
        return WEXITSTATUS(stat);
}
void child(){
		

	for(int i=1;i<=stages;i++)	
	{
		char message[32];
		sprintf(message,"%s%d","STAGE",i);
		process_stage(i);
 		//Stage 1 done	
		write_message(shmid,message);
		usleep(10000);
		
	}

        exit(stages); 
}
void parent()
{
	char *last_message=NULL;
	char *message=NULL; 

	for(int i=1;i<=stages;i++)	
	{
		printf("Waiting for the child to finish the stage:%d\n",i);
		fflush(stdout);
		//usleep(1000);
		//The following loop is a hack to ensure parent waits for the
   		//new message from child in a loop. In future we will use
		//Semaphore to ensure synchronization.
		do
		{

			usleep(1000);

			last_message=message;
			message=read_message(shmid,32);	

		}while((last_message==NULL && message == NULL) 
		|| (last_message!=NULL && strcmp(last_message,message)==0));

		printf("STAGE completed:%s\n",message);


		fflush(stdout);
	}

	printf("Child exited with status:%d\n",get_child_exit_status());
	remove_shared_mem(shmid);
}

void process_stage(int stage)
{
	//
	//Some complex logic is executed here, we just do a printf for now
	//
	printf("Procesing stage%d\n",stage);
	usleep(1);
	fflush(stdout);
}

int main(int argc, char* argv[]) 
{ 
	pid_t cid; 
	shmid = create_shared_mem(100); 
	if(shmid == 0 )
	{
		printf("Shared Mem creation failed\n");
	}
	scanf("%d",&stages);
	cid = fork(); 

	// Parent process 
	if (cid == 0) 
	{ 
		child();
	} else if(cid > 0 )
	{
		parent();
	}
} 
